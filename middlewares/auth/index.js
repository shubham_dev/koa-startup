const appService = require("../../services");

const User = require('../../dbmodels/models/user');
module.exports = (app) => {
    try {
        console.log("Authenticating Request Setup");
        app.use(async (ctx, next) => {
            let token = ctx.request.header['authorization'].replace('Bearer ', '');
            let authorise = appService['jwtAuth'].mainAuth.verify(token);
            if (!authorise) {
                throw Error('Error in validating the user');
            }
            else {
                ctx.request.passport = {};
                ctx.request.passport.user = await User.findOne({ '_id': authorise.payload });
            }
            await next();
        });
    }
    catch (err) {
        console.error("Error in Auhtenticating request : ", err);
    }
};
