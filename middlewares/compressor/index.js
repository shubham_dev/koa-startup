'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const compress = require("koa-compress");
module.exports = (app) => {
    try {
        app.use(compress());
    }
    catch (err) {
        console.error("Error in compressing : ", err);
    }
};
