'use strict';
let cors = require('@koa/cors');
module.exports = (app) => {
    console.log("CORS Setup");
    app.use(cors({
        maxAge: 3600,
        credentials: true,
        allowMethods: 'GET, HEAD, OPTIONS, PUT, POST, DELETE',
        allowHeaders: 'Access-Control-Allow-Origin, Access-Control-Expose-Headers, Origin, X-Requested-With, Content-Type, Accept, Authorization, Access-Control-Allow-Credentials'
    }));
};
