'use strict';
module.exports = (app) => {
    try {
        app.use(async (ctx, next) => {
            ctx.req.connection.setTimeout(0);
            ctx.req.connection.setNoDelay(true);
            await next();
        });
    }
    catch (err) {
        console.error("Error in creating timeout and delay : ", err);
    }
};
