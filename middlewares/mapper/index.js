'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const fs = require("fs");
let router = require('koa-router'), publicAPI = router(), securedAPI = router();
module.exports = () => {
    fs.readdirSync('./controllers').forEach(file => {
        if (!file.endsWith('.js')) {
            return;
        }
        let controller = require('../../controllers/' + file);
        if (controller.publicRoute) {
            controller.publicRoute(publicAPI);
        }
        if (controller.securedRoute) {
            controller.securedRoute(securedAPI);
        }
    });
    return { "public": publicAPI, "secured": securedAPI };
};
