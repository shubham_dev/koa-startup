'use strict';
let utilities = require('utilities');
let parse = require('./parser');
let cors = require('./cors');
let route = require('./router');
let auth = require('./auth');
let timer = require('./timer');
let compresser = require('./compressor');
let mapper = require('./mapper')();
module.exports = function (app, config) {
    cors(app);
    timer(app);
    compresser(app);
    parse(app, utilities.config);
    route({ "app": app, "config": config, "mapper": mapper });
    auth(app, config);
    route({ "app": app, "config": config, "mapper": mapper, "auth": true });
};
