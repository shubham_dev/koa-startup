'use strict';

const Koa = require('koa');
const app = module.exports = new Koa();

const utilities = require('utilities');
const dbInitter = require('./dbmodels');
const config = utilities.config;

// dbInitter.init(utilities); // initailising the db models // utilities is define to use logger

const middleware = require('./middlewares');
middleware(app, config);

let startDb = new Promise(
    function(resolve, reject) {
        dbInitter.initDatabase(utilities).then(function() {
            console.info("Mongoose Initiated Successfully");
            resolve(true);
        }).catch(function(error) {
            console.error("Error innpm  initailising DB : ", error);
            reject(error);
        });
    },
);

startDb.then(function() {
    return new Promise(function(resolve, reject) {
        console.info('Starting Message server . . . ');
        if (!module.parent) {
            app.server = app.listen(config.systemConfig.app.port);
            resolve(true);
        } else {
            reject(new Error("error in starting"));
        }
    });
}).then(function() {
    console.info("Started Message Server at : ", config.systemConfig.app.host, ":", config.systemConfig.app.port);
    process.on('uncaughtException', (err) => {
        console.error(`Caught exception: ${err}\n`);
    });
}).catch(function(error) {
    console.error("Error in starting Message Server : ", error);
});
