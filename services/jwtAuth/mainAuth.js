const fs = require('fs');
const jwt = require('jsonwebtoken');
var privateKEY = fs.readFileSync((__dirname + '/../../keys/private.key'), 'utf8');
var publicKEY = fs.readFileSync((__dirname + '/../../keys/public.key'), 'utf8');
module.exports = {
    sign: (payload) => {
        var signOptions = {
            expiresIn: "1d",
            algorithm: "RS256"
        };
        return jwt.sign({ payload }, privateKEY, signOptions);
    },
    verify: (token) => {
        var verifyOptions = {
            expiresIn: "1d",
            algorithm: ["RS256"]
        };
        try {
            return jwt.verify(token, publicKEY, verifyOptions);
        }
        catch (err) {
            return false;
        }
    }
};
