/* eslint-disable no-useless-catch */
const fs = require('fs');
function generateServiceHandlers(path) {
    try {
        let object = {};
        fs.readdirSync(path).forEach(function (item) {
            try {
                if (fs.statSync(`${path}/${item}`).isDirectory()) {
                    object[item.toString()] = generateServiceHandlers(`${path}/${item}`);
                }
                else {
                    if (item !== 'index.js') {
                        if (/\.js$/.test(item)) {
                            object[item.substring(0, item.length - 3)] = require(`${path}/${item}`);
                        }
                    }
                }
            }
            catch (err) {
                throw err;
            }
        });
        return object;
    }
    catch (err) {
        throw err;
    }
}
module.exports = generateServiceHandlers(__dirname);
