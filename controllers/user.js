const User = require('../dbmodels/models/user');

exports.securedRoute = (app) => {
    app.get('/v1/user/me', getUserDetails);
    
    app.post('/users/logout', logoutUser);
    
    app.delete('/v1/user/delete:id', deleteUser);
    app.patch('/v1/user/update:id', updateUser);
};

exports.publicRoute = (app) => {
    app.post('/v1/user/login', loginUser);
    app.post('/v1/user/create', createUser);
};

const getUserDetails = async (ctx) => {
    try {
        ctx.body = ctx.request.passport.user;
        ctx.status = 200;
    } catch (err) {
        console.log('here is the error');
        throw err;
    }
};

const createUser = async (ctx) => {
    const user = new User(ctx.request.fields);
    try {
        await user.save();
        let token = await user.generateAuthToken();
     
        ctx.status = 201;
        ctx.body = { user : user, token : token };
    } catch (err) {
        ctx.status = 500;
        ctx.body = err;
    }
};

const loginUser = async (ctx) => {
    const body = ctx.request.fields;
    try {
        const user = await User.findByCredentials(body.email, body.password);
        const token = await user.generateAuthToken();
     
        ctx.body = { user: user, token: token };
        ctx.status = 200;
    } catch (err) {
        ctx.status = 500;
        ctx.body = err;
    }
};

const deleteUser = async (ctx) => {
    let user = ctx.passport.user;
    try {
        await user.remove();

        ctx.status = 200;
        ctx.body = {'msg' : 'user deleted successfully'};
    } catch (err) {
        ctx.status = 500;
        ctx.body = err;
    }
};

const updateUser = async (ctx) => {
    let user = ctx.passport.user;
    const body = ctx.request.fields;
    const updates = Object.keys(body);
    const allowedUpdates = ['name', 'email', 'password', 'age']
    const isValidOperation = updates.every((update) => allowedUpdates.includes(update))

    if (!isValidOperation) {
        ctx.status = 400;
        ctx.body = { error: 'Invalid updates!' }
    }

    try {
        updates.forEach((update) => user[update] = body[update]);
        await user.save();
        ctx.status = 200;
        ctx.body = user;
    } catch (err) {
        ctx.status = 500;
        ctx.body = err;
    }
};

const logoutUser = async (ctx) => {
    try {
        let user = ctx.passport.user;
        user.tokens = user.tokens.filter((token) => {
            return token.token !== ctx.request.token;
        })
        await user.save();
        ctx.status = 200;
        ctx.body = {'msg' : 'logout successfully'};
    } catch (err) {
        ctx.status = 500;
        ctx.body = err;
    }
};