const Event = require('../dbmodels/models/event');

exports.securedRoute = (app) => {
  app.get('/v1/event/:id', getEventDetails);
  app.get('/v1/event/all', getAllEvents);

  app.post('/v1/event/add', addEvent);
  
  app.delete('/v1/event/delete', deleteEvent);
};

const addEvent = async (ctx) => {
  try {
    const eventDetails = ctx.request.fields;
    const user = ctx.request.passport.user;
    const data = await new Event({...eventDetails, 'owner': user._id}).save();
    ctx.body = data;
    ctx.status = 200;
  } catch (err) {
    console.log('error in adding new contacts', err);
  }
};

const getEventDetails = async (ctx) => {
  const _id = ctx.request.fields;
  const user = ctx.request.passport.user;

  try {
    const task = await Event.findOne({ _id, owner: user._id })

    if (!task) {
      ctx.status = 400;
      ctx.body = {'msg' : 'event not found'};
    }

    ctx.status = 200;
    ctx.body = task;
  } catch (err) {
    ctx.status = 500;
    ctx.body = err;
  }
};

const getAllEvents = async (ctx) => {
  const user = ctx.request.passport.user;
  try {
    ctx.status = 200;
    ctx.body = await Event.find({owner: user._id});
  } catch (err) {
    ctx.status = 500;
    ctx.body = err;
  }
};

const deleteEvent = async (ctx) => {
  try {
    const body = ctx.request.fields;

    ctx.status = 200;
    ctx.body = await Event.remove({_id: body._id});
  } catch (err) {
    ctx.status = 500;
    ctx.body = err;
  }
};
