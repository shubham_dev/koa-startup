const mongoose = require("mongoose");

const Schema = mongoose.Schema;

const EventSchema = new Schema({
  eventData: {
    type: Object,
    required: true,
  },
  owner: {
    type: mongoose.Schema.Types.ObjectId,
    required: true,
    ref: 'User'
  },
  created: {
    type: Date,
    required: false,
    default: Date.now,
  },
  modified: {
    type: Date,
    required: false,
    default: Date.now,
  },
});
mongoose.model('Eventdata', EventSchema);

const Event = mongoose.model('Event', EventSchema);
module.exports = Event;