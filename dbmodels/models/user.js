const mongoose = require('mongoose');
const validator = require('validator');
const jwtService = require('../../services/jwtAuth');
const bcrypt = require('bcryptjs');
const Event = require('./event');

const Schema = mongoose.Schema;
const UserSchema = new Schema({
	name: {
		type: String,
		required: true,
		trim: true,
	},
	email: {
		type: String,
		unique: true,
		required: true,
		trim: true,
		lowercase: true,
		validate(value) {
			if (!validator.isEmail(value)) {
				throw Error('Invalid Email Please enter correct email');
			}
		},
	},
	password: {
		type: String,
		required: true,
		validate(value) {
			if (value.length < 8) {
				throw Error('Password should be greater than 8 characters');
			}
		},
	},
	tokens: [{
		token: {
			type: String,
			required: true,
		},
	}],
	created: {
		type: Date,
		required: false,
		default: Date.now,
	},
	modified: {
		type: Date,
		required: false,
		default: Date.now,
	},
});

UserSchema.virtual('events', {
	ref: 'Event',
	localField: '_id',
	foreignField: 'owner'
})

UserSchema.pre('remove', async function(next) {
	const user = this;
	await Event.deleteMany({ owner: user._id });

	next();
});

UserSchema.methods.toJSON = function () {
	const user = this;
	const userObject = user.toObject();

	delete userObject.password;
	delete userObject.tokens;

	return userObject;
}

UserSchema.methods.generateAuthToken = async function() {
	const user = this;
	const token = await jwtService.mainAuth.sign(user['_id']);

	user.tokens = user.tokens.concat({ token });
	await user.save();

	return token;
};

UserSchema.statics.findByCredentials = async (email, password) => {
	const user = await User.findOne({ 'email': email });

	if (!user) {
		throw new Error('Unable to login');
	}

	const isMatch = await bcrypt.compare(password, user.password);

	if (!isMatch) {
		throw new Error('Unable to login');
	}

	return user;
}

// Hash the plain text password before saving
UserSchema.pre('save', async function (next) {
	const user = this;

	if (user.isModified('password')) {
		user.password = await bcrypt.hash(user.password, 8);
	}

	next();
})

const User = mongoose.model('User', UserSchema);
module.exports = User;
