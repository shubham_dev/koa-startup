'use strict';
let baseConfig = require('./baseConfig');
let path = require('path');
let systemConfig = {
    "dev": {
        "app": {
            "host": 'localhost',
            "port": '3000',
            "downloadDir": path.join(__dirname + '/../../downloads'),
            "cookieOptions": {
                "domain": `http://${this.host}/${this.port}`
            }
        },
        "mongo": {
            seed: false,
            debug: true,
            dbname: 'organize-event',
            host: '127.0.0.1',
            port: 27017,
            getURI: function (exFlag) {
                return `mongodb://${this.host}:${this.port}/${this.dbname}`;
            }
        },
        "log": {
            level: 'debug',
            path: __dirname + '/../../logs/msgapp.log'
        }
    }
};
module.exports = Object.assign(Object.assign({}, baseConfig), systemConfig[baseConfig.app.env || 'dev']);
